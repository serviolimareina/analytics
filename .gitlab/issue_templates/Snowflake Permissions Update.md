<!-- format should be something like 'user [de]provisioning - firstname last initial' -->
<!-- example: user provisioning - John S -->


Source Access Request: <!-- link to source  Access Request issue, it should be approved and ready for provisioning -->

# Creating a New User in Snowflake

- [ ] Create user using SECURITYADMIN role
- [ ] Create user specific role
- [ ] Assign user specific role
- [ ] Add to [okta-snowflake-users google group](https://groups.google.com/a/gitlab.com/g/okta-snowflake-users/members)
- [ ] Verify grants
- [ ] Update `roles.yml` and add a comment with a access request URL

/label ~Provisioning ~Snowflake ~"Team::Data Platform"  ~"Priority::1-Ops"
