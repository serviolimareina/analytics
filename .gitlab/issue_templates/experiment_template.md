<!---
This issue is for experiments that require analysis from the Product Analysis team
---->

#### What is the business question you are trying to answer?

Example: Is there a relationship between the day of the week that deals close and the ability of the account manager to upsell them in the first month?

#### What is the impact of this question and how will it help the company?


#### Please link to related issues to provide context on this experiment.

Link to Parent Experiment Issue:

Link to Event Tracking Issue:

Other Relevant Links: 

#### Please define the components of any calculations needed for this analysis.

For example, what is the numerator and denominator?

#### If applicable, please provide any detail on event sequences of interest.

For example, if this experiment is tracking a conversion funnel through 4 different events, what order of events is desired to be tracked.

#### Please list any unique/special event names and provide any relevant context around this event


#### Please link to where this (or these) performance indicator/s are defined in the handbook. 


#### What is the source of data behind the experiment?


#### Are there any forms of granularity needed in the analysis?


#### Please provide an example report to outline the key events and/or metrics needed for this experiment analysis (see example below)

|                                                             | Test    | Control |      
|-------------------------------------------------------------|---------|---------|
| (Example) number of project page views                                | 20000   | 20000   |
| (Example) number of clicks on the integration CTA                     | 1000    | 0       |
| (Example) number of view of /-/settings/integrations for the project  | 1500    | 1000    |
| (Example) number of projects with an integration set up               | 750     | 500     | 


#### What is the visualization you are trying to create?

For example: table, charts, graphs

#### Any caveats or details that may be helpful?


#### Target Dates and Statuses for Tasks

This section is to help provide clarity into the current stage of the experiment. Please alter table as needed to fit the needs for the specific experiment. The table can be updated as the experiment progresses.

| Task | DRI or Team Responsible | Status | Notes |
| ------ | ------ | ------ | ------ |
|Tracking events implemented|Engineering| 
|Experiment events identified in Sisense|Product Analysis|
|Events run in staging|Product/Engineering|
|Staging events seen in staging|Product Analysis|
|Initial Sisense Dashboard Built on Staging Data|Product Analysis|
|Experiment set live on production|Product/Engineering|
|Sisense Dashboard set to show Production Data|Product Analysis|
|Experiment has concluded|Product/Engineering|
|Analysis of Experiment Results|Product Analysis|

/label ~"Data Team" ~"product analysis" ~"Experiment"
