{% docs pump_marketing_contact %}

a copy of mart_marketing_contact for sending to Marketo for use in email campaigns

{% enddocs %}

{% docs pump_subscription_product_usage %}

A copy of `subscription_product_usage_data` model for sending to Salesforce

{% enddocs %}

{% docs pump_marketing_premium_to_ultimate %}

A subset of pump_marketing_contact, with all of the same columns, that supports the premium to ultimate email marketing campaign and is the proof of concept pump for the email marketing data pump.

{% enddocs %}